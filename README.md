# Debian installer for domU

## Conventions

I'm using `/var/lib/xen/<release>-<arch>` to hold the installer's kernel and initrd files.

VM config files are in `/etc/xen/vm/<vm-name>.config`

In the rest of this writeup the release is Bookworm, arch is amd64, and the new VM will be named wurm.

Much of this MUST be done as root, of course, so I assume commands are run by root.  Unless you have a yearning to type _sudo_ over and over and over and ...

Since this is all about Debian domUs, I show only PV[H] mode operation.  I expect that if you needed to run the domU under full virtualization you would want to install it that way to ensure that everything was configured properly.  But I have zero experience to share about that.

## Prepare to direct boot the installer

Create the directory `/var/lib/xen/<release>-<arch>`, copy `refresh.sh` into it, and edit the script for the package repository, release and arch you want (they're all part of that long URL).  Run `sh refresh.sh` to download the kernel and initrd files.

In `/etc/xen/vm`, create `wurm.cfg` as you would for a normal Linux domU, but with the `type` and boot parameters changed to direct boot using the installer files.
```
name="wurm"

# normal boot of installed domU
#type="pvh"
#kernel="/usr/lib/grub-xen/grub-i386-xen_pvh.bin"

# installation by direct boot
type="pv"
kernel="/var/lib/xen/bookworm-amd64/linux"
ramdisk="/var/lib/xen/bookworm-amd64/initrd.gz"
cmdline="debian-installer/exit/always_halt=true -- console=hvc0"
```

Memo: I'm not sure that you have to use "pv" for the installation - like the exact contents of the cmdline this is a holdover from previous versions, and since it continues to work for me...

## Boot the domU and do the installer

`xl create -c /etc/xen/vm/wurm.cfg`

The installer boots and runs as usual.  After it's done it should exit and return your console or xterm to the host's shell.

## Edit the config to boot from the domU's kernel and initrd (viz., "normal boot")

I tend to just edit the comment characters on the boot lines, but now that you have the reference above available, you can feel free to just remove the installer lines.

## Boot the domU normally

`xl create -c /etc/xen/vm/wurm.cfg`

Okay, this may be just me, but I normally want to set a static address and such, and using `-c` saves fumbling with finding the dynamic address just to login using it the once.

## Other notes

With Bookworm, Debian now installs a version of systemd which applies Unstable Interface Names to Xen's virtual interfaces.  God knows why - must have had a brain fart and were too embarassed to revert it, like a cat pretending that's not its puddle over there on the linoleum.  So now all my VMs get the `net.ifnames=0`, just like most of the bare metal installs have.  _Remember to run `update-grub` to push the change into grub.cfg!_

I swear I'm going to file a bug report about having the damned apt-daily crap default enabled this time.  Right after I `systemctl {stop,disable} apt-daily{,-upgrade}.timer`.  Of course systemctl isn't smart enough to expand braces expressions like ls, so it takes more typing.




